"""
My first application
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW
import httpx
import asyncio

MAX_PROGRESSBAR_VALUE = 100
class SimpleNotes(toga.App):

    def startup(self):

        self.label_input = toga.TextInput(style=Pack(flex=1))
        self.progress_adder = toga.ProgressBar(max=MAX_PROGRESSBAR_VALUE)
        self.progress_complete_status = toga.Label('Progress incomplete', style=Pack(padding=(0, 5)))
        self.main_box = toga.Box(
            style=Pack(direction=COLUMN),
            children=[
                toga.Box(
                    style=Pack(direction=ROW, padding=5),
                    children=[
                        toga.Label('Your name: ', style=Pack(padding=(0, 5))),
                        self.label_input
                        ]
                ),
                toga.Button(
                    'Say Hello!',
                    on_press=self.say_hello,
                    style=Pack(padding=5)
                ),
                toga.Box(
                    style=Pack(direction=ROW,flex=1),
                    children=[
                        toga.Switch(
                            "toggle mode",
                            on_toggle=self.toggle_running
                        ),
                        self.progress_adder,
                        self.progress_complete_status
                    ]

                )
            ]
        )


        self.main_window = toga.MainWindow(title=self.formal_name)
        self.main_window.content = self.main_box
        self.main_window.show()

    async def say_hello(self, widget):
        if self.label_input.value:
            name = self.label_input.value
        else:
            name = 'stranger'

        async with httpx.AsyncClient() as client:
            response = await client.get("https://jsonplaceholder.typicode.com/posts/42")

        payload = response.json()

        self.main_window.info_dialog(
            "Hello, {}".format(name),
            payload["body"],
        )

    async def toggle_running(self, switch, **kw):

        if self.progress_adder.is_determinate:
            for i in range(1, MAX_PROGRESSBAR_VALUE):
                self.progress_adder.value = i + 1
                await asyncio.sleep(0.1)
                if self.progress_adder.value == MAX_PROGRESSBAR_VALUE:
                    self.main_window.info_dialog("Status","Progress complete")
                    self.progress_complete_status.text='Progress complete'



def main():
    return SimpleNotes()
