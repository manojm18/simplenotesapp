# Simple App using [Toga](https://toga.readthedocs.io/) & [BeeWare](https://beeware.org/toga/)

Reference: https://docs.beeware.org/en/latest/index.html

## Environment setup on Windows

### Virtual Env
In an empty project directory, create a python virtualenv and activate it:
- `python -m venv venv`
- `.\venv\Scripts\activate.bat`

### Dependencies
Install Briefcase
- `python -m pip install briefcase`

### Bootstrap a new project
- `briefcase new`
then follow on screen prompt to fill in details about your project.
- `cd <YOUR_PROJECT_DIR>`

### Run in developer mode

- `briefcase dev` will run your app in dev mode

### For Android
From `<YOUR_PROJECT_DIR>`, run
- `briefcase create android`
- `briefcase build android`
- `briefcase run android`
    - select the device from prompt in case you have a android phone connected with USB debugging enabled

#### Updating your code 
In case of updating your python code, or updating dependencies, update your [requirements.txt](./requirements.txt) and [pyproject.toml](./simplenotes/pyproject.toml)
- `briefcase update android -d`
- `briefcase build android`
- `briefcase run android`